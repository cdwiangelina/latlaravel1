<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form - Christina Dwi Angelina</title>
</head>
<body>

    <!-- Christina Dwi Angelina -->

    <div>
        <h1> Buat Account Baru!</h1>

        <h3> Sign Up Form</h3> 

        <form action="/kirim" method="POST">
            @csrf
            
            <label for="firstName"> First Name : </label> <br><br>
            <input type="text" name="firstName" id="firstName"> <br><br>

            <label for="lastName"> Last Name : </label> <br><br>
            <input type="text" name="lastName" id="lastName"> <br><br>

            <label> Gender : </label> <br> <br>
            <input type="radio" value="Male" name="gender" id="Male">
            <label for="Male"> Male </label> <br>
            <input type="radio" value="Female" name="gender" id="Female">
            <label for="Female"> Female </label> <br>
            <input type="radio" value="Other" name="gender" id="Other">
            <label for="Other"> Other </label> <br><br>

            <label> Nationality : </label> <br><br>
            <select name="nasionality" id="nasionality">
                <option value="indonesian"> Indonesian </option>
                <option value="singapore">Singapore</option>
                <option value="malaysian"> Malaysian </option>
                <option value="australian"> Australian</option>
            </select> 

            <br><br>
            <label> Language Spoken : </label> <br><br>
            <input type="checkbox" name="language" value="indonesia" id="indonesia">
            <label for="indonesia"> Bahasa Indonesia</label> <br>
            <input type="checkbox" name="language" value="english" id="english">
            <label for="english"> English</label> <br>
            <input type="checkbox" name="language" value="other" id="other2">
            <label for="other2"> Other </label> <br><br>

            <label for="bio"> Bio : </label> <br><br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
            <br>
            <input type="submit" value="kirim">
        </form>
    </div>

</body>
</html>