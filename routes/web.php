<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', 'HomeController@Home');

Route::get('/', 'HomeController@Home');
Route::get('/register', 'AuthController@register');
Route::get('/welcome', 'AuthController@welcome');
Route::post('/kirim', 'AuthController@kirim');


// Route::get('/test/{angka}', function($angka){
//     return view('test', ["angka" => $angka]);
// });

// Route::get('/halo', function(){
//     return "Hello Word";
// });

// Route::get('user/{id}', function($id){
//     return "halo $id";
// });

// Route::get('/form', 'LatihanController@form');

// Route::get('/sapa', 'LatihanController@sapa');

// Route::post('/sapa', 'LatihanController@sapa_post');